<?php

namespace Mosaic\CMSBundle\Tests;

use Mosaic\CMSBundle\Model\Page;

class TestPage extends Page
{
    public function setId($id)
    {
        $this->id = $id;
    }
}