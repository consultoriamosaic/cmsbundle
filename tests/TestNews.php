<?php

namespace Mosaic\CMSBundle\Tests;

use Mosaic\CMSBundle\Model\News;

class TestNews extends News
{
    public function setId($id)
    {
        $this->id = $id;
    }
}