<?php

namespace Mosaic\CMSBundle\Tests\Doctrine;

use Mosaic\CMSBundle\Doctrine\PageManager;
use Mosaic\CMSBundle\Model\Page;


class PageManagerTest extends \PHPUnit_Framework_TestCase
{
    const PAGE_CLASS = 'Mosaic\CMSBundle\Tests\Doctrine\DummyPage';

    private $manager;
    private $om;
    private $repository;

    protected function setUp()
    {
        if (!interface_exists('Doctrine\Common\Persistence\ObjectManager')) {
            $this->markTestSkipped('Doctrine Common has to be installed for this test to run.');
        }

        $class = $this->getMock('Doctrine\Common\Persistence\Mapping\ClassMetadata');
        $this->om = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $this->repository = $this->getMockBuilder('Mosaic\CMSBundle\Repository\DoctrinePageRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->om->expects($this->any())
            ->method('getClassMetadata')
            ->with($this->equalTo(static::PAGE_CLASS))
            ->will($this->returnValue($class));
        $class->expects($this->any())
            ->method('getName')
            ->will($this->returnValue(static::PAGE_CLASS));

        $this->manager = $this->createPageManager($this->repository, $this->om, static::PAGE_CLASS);
    }

    public function testGetClass()
    {
        $this->assertEquals(static::PAGE_CLASS, $this->manager->getClass());
    }

    public function testDeletePage()
    {
        $page = $this->getPage();
        $this->repository->expects($this->once())->method('delete')->with($this->equalTo($page));

        $this->manager->deletePage($page);
    }

    public function testUpdatePage()
    {
        $page = $this->getPage();
        $this->repository->expects($this->once())->method('save')->with($this->equalTo($page));

        $this->manager->updatePage($page);
    }

    public function testFindPageBy()
    {
        $criteria = array("slug" => "");
        $this->repository->expects($this->once())->method('by')->with($this->equalTo($criteria))->will
        ($this->returnValue(array()));

        $this->manager->findPageBy($criteria);
    }

    public function testFindAllPages()
    {
        $this->repository->expects($this->once())->method('all')->will($this->returnValue(array()));

        $this->manager->findAllPages();
    }

    protected function createPageManager($pageRepository, $objectManager, $pageClass)
    {
        return new PageManager($pageRepository, $objectManager, $pageClass);
    }

    protected function getPage()
    {
        $class = static::PAGE_CLASS;

        return new $class;
    }

}

class DummyPage extends Page
{
}
