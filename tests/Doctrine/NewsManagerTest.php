<?php

namespace Mosaic\CMSBundle\Tests\Doctrine;


use Mosaic\CMSBundle\Model\News;
use Mosaic\CMSBundle\Doctrine\NewsManager;

class NewsManagerTest extends \PHPUnit_Framework_TestCase
{
    const NEWS_CLASS = 'Mosaic\CMSBundle\Tests\Doctrine\DummyNews';

    private $manager;
    private $om;
    private $repository;

    protected function setUp()
    {
        if (!interface_exists('Doctrine\Common\Persistence\ObjectManager')) {
            $this->markTestSkipped('Doctrine Common has to be installed for this test to run.');
        }

        $class = $this->getMock('Doctrine\Common\Persistence\Mapping\ClassMetadata');
        $this->om = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $this->repository = $this->getMockBuilder('Mosaic\CMSBundle\Repository\DoctrineNewsRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->om->expects($this->any())
            ->method('getClassMetadata')
            ->with($this->equalTo(static::NEWS_CLASS))
            ->will($this->returnValue($class));
        $class->expects($this->any())
            ->method('getName')
            ->will($this->returnValue(static::NEWS_CLASS));

        $this->manager = $this->createNewsManager($this->repository, $this->om, static::NEWS_CLASS);
    }

    public function testGetClass()
    {
        $this->assertEquals(static::NEWS_CLASS, $this->manager->getClass());
    }

    public function testdeleteNews()
    {
        $news = $this->getNews();
        $this->repository->expects($this->once())->method('delete')->with($this->equalTo($news));

        $this->manager->deleteNews($news);
    }

    public function testupdateNews()
    {
        $news = $this->getNews();
        $this->repository->expects($this->once())->method('save')->with($this->equalTo($news));

        $this->manager->updateNews($news);
    }

    public function testFindNewsBy()
    {
        $criteria = array("slug" => "");
        $this->repository->expects($this->once())->method('by')->with($this->equalTo($criteria))->will
        ($this->returnValue(array()));

        $this->manager->findNewsBy($criteria);
    }

    public function testfindAllNews()
    {
        $this->repository->expects($this->once())->method('all')->will($this->returnValue(array()));

        $this->manager->findAllNews();
    }

    protected function createNewsManager($newsRepository, $objectManager, $newsClass)
    {
        return new NewsManager($newsRepository, $objectManager, $newsClass);
    }

    protected function getNews()
    {
        $class = static::NEWS_CLASS;

        return new $class;
    }

}

class DummyNews extends News
{
}
