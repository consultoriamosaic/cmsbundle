<?php

namespace Mosaic\CMSBundle\Tests\Model;

use Mosaic\CMSBundle\Model\Locale;

class LocaleTest extends \PHPUnit_Framework_TestCase
{

    private static $localeArgs = array(
        'code' => 'es_ES'
    );

    public function testCode()
    {
        $locale = $this->getLocale(' es_ES   ');
        $this->assertNotNull($locale->getCode());

        $this->assertEquals($locale->getCode(), $locale->canonicalizeCode($locale->getCode()));
        $this->assertEquals($locale->getCode(), trim($locale->getCode()));
    }

    public function testEnabled()
    {
        $locale = $this->getLocale();
        $this->assertFalse($locale->isEnabled());

        $locale->setEnabled(true);
        $this->assertTrue($locale->isEnabled());
    }

    /**
     * @param string Locale::code
     * @return Locale
     */
    protected function getLocale($locale = null)
    {
        return new Locale(isset($locale) ? $locale : self::$localeArgs['code']);
    }
}
