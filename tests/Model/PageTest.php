<?php

namespace Mosaic\CMSBundle\Tests\Model;

use Mosaic\CMSBundle\Model\Page;

class PageTest extends \PHPUnit_Framework_TestCase
{
    
    public function testTitle()
    {
        $page = $this->getPage();
        $this->assertNull($page->getTitle());

        $page->setTitle('Not another Symfony site.');
        $this->assertEquals('Not another Symfony site.', $page->getTitle());
    }

    public function testSlug()
    {
        $page = $this->getPage();
        $this->assertNull($page->getSlug());

        $page->setSlug('not-another-symfony-site');
        $this->assertEquals('not-another-symfony-site', $page->getSlug());
    }

    public function testPublic()
    {
        $page = $this->getPage();
        $this->assertTrue($page->isPublic());
    }

    public function testCreationDate()
    {
        $beforeCreationDate = new \DateTime('now');
        $page = $this->getPage();

        $this->assertGreaterThanOrEqual($page->getCreationDate(), $beforeCreationDate);

    }

    /**
     * @return Page
     */
    protected function getPage()
    {
        return $this->getMockForAbstractClass('Mosaic\CMSBundle\Model\Page');
    }
}
