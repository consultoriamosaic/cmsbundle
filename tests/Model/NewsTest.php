<?php

namespace Mosaic\CMSBundle\Tests\Model;


use Mosaic\CMSBundle\Model\News;

class NewsTest extends \PHPUnit_Framework_TestCase
{

    public function testTitle()
    {
        $news = $this->getNews();
        $this->assertNull($news->getTitle());

        $news->setTitle('Not another Symfony news.');
        $this->assertEquals('Not another Symfony news.', $news->getTitle());
    }

    public function testSlug()
    {
        $news = $this->getNews();
        $this->assertNull($news->getSlug());

        $news->setSlug('not-another-symfony-news');
        $this->assertEquals('not-another-symfony-news', $news->getSlug());
    }

    public function testPublished()
    {
        $news = $this->getNews();
        $this->assertFalse($news->isPublished());
    }

    public function testCreationDate()
    {
        $beforeCreationDate = new \DateTime('now');
        $news = $this->getNews();

        $this->assertGreaterThanOrEqual($news->getCreationDate(), $beforeCreationDate);
    }

    /**
     * @return News
     */
    protected function getNews()
    {
        return $this->getMockForAbstractClass('Mosaic\CMSBundle\Model\News');
    }
}
