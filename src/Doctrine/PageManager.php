<?php

namespace Mosaic\CMSBundle\Doctrine;

use Mosaic\CMSBundle\Model\PageInterface;
use Mosaic\CMSBundle\Model\PageManager as BasePageManager;
use Mosaic\CMSBundle\MosaicCMSEvents;
use Mosaic\CMSBundle\Repository\PageRepositoryInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PageManager extends BasePageManager
{
    /**
     * @var string
     */
    protected $class;
    /**
     * @var PageRepositoryInterface
     */
    protected $repository;
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * PageManager constructor.
     * @param PageRepositoryInterface $pageRepository
     * @param EventDispatcherInterface $dispatcher
     * @param ObjectManager $objectManager
     * @param string $class
     */
    public function __construct(
        PageRepositoryInterface $pageRepository,
        EventDispatcherInterface $dispatcher,
        ObjectManager $objectManager,
        $class
    ) {
        $this->repository = $pageRepository;
        $this->dispatcher = $dispatcher;

        $metadata = $objectManager->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function deletePage(PageInterface $page)
    {
        $this->repository->delete($page);
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param PageInterface $page
     */
    public function updatePage(PageInterface $page)
    {
        $page->setUpdateDate();
        $this->repository->save($page);
    }

    /**
     * {@inheritdoc}
     */
    public function findPageBy(array $criteria)
    {
        $page = $this->repository->by($criteria);

        return $page;

    }

    /**
     * {@inheritdoc}
     */
    public function findAllPages()
    {
        return $this->repository->all();
    }

    /**
     * {@inheritdoc}
     */
    public function getPage($machineName)
    {
        $page = $this->findPageByMachineName($machineName);

        if (!$page instanceof $this->class) {
            $this->dispatcher->dispatch(MosaicCMSEvents::PAGE_NOT_FOUND);
        }

        return $page;
    }

    /**
     * {@inheritdoc}
     */
    public function getPageByMachineNamePrefix($machineNamePrefix)
    {
        return $this->repository->getPage($machineNamePrefix);
    }

    /**
     * {@inheritdoc}
     */
    public function getAssociatedBlocks($machineName)
    {
        $associatedBlocks = [];
        $blocks = $this->repository->getBlocks($machineName);
        foreach($blocks AS $block) {
            $associatedBlocks[$block->getMachineName()] = $block;
        }

        return $associatedBlocks;
    }

}