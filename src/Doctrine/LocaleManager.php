<?php

namespace Mosaic\CMSBundle\Doctrine;

use Mosaic\CMSBundle\Model\LocaleInterface;
use Mosaic\CMSBundle\Model\LocaleManager as BaseLocaleManager;
use Doctrine\Common\Persistence\ObjectManager;


class LocaleManager extends BaseLocaleManager
{
    protected $objectManager;
    protected $repository;
    protected $class;

    /**
     * LocaleManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager, $class)
    {
        $this->objectManager = $objectManager;
        $this->class = $class;
        $this->repository = $objectManager->getRepository($class);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteLocale(LocaleInterface $locale)
    {
        $this->objectManager->remove($locale);
        $this->objectManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function enableLocale(LocaleInterface $locale)
    {
        $locale->setEnabled(true);
        $this->updateLocale($locale);
    }

    /**
     * {@inheritdoc}
     */
    public function disableLocale(LocaleInterface $locale)
    {
        $locale->setEnabled(false);
        $this->updateLocale($locale);
    }

    /**
     * {@inheritdoc}
     */
    public function updateLocale(LocaleInterface $locale)
    {
        $this->objectManager->persist($locale);
        $this->objectManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getEnabledLocales()
    {
        return $this->repository->findBy(array('enabled' => true));
    }

    /**
     * {@inheritdoc}
     */
    public function getAllLocales()
    {
        return $this->repository->findAll();
    }

    /**
     * {@inheritdoc}
     */
    public function findLocaleBy($criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->class;
    }


}