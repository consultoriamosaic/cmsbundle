<?php

namespace Mosaic\CMSBundle\Doctrine;

use Mosaic\CMSBundle\Model\SliderPhotoInterface;
use Mosaic\CMSBundle\Model\SliderManager AS BaseSliderManager;
use Mosaic\CMSBundle\Repository\SliderRepositoryInterface;

class SliderManager extends BaseSliderManager
{
    /**
     * @var string
     */
    protected $class;
    /**
     * @var SliderRepositoryInterface
     */
    protected $repository;

    /**
     * SliderManager constructor.
     * @param SliderRepositoryInterface $repository
     * @param string $class
     */
    public function __construct(SliderRepositoryInterface $repository, $class)
    {
        $this->repository = $repository;
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteSliderPhoto(SliderPhotoInterface $sliderPhoto)
    {
        $this->repository->delete($sliderPhoto);
    }

    /**
     * {@inheritdoc}
     */
    public function updateSliderPhoto(SliderPhotoInterface $sliderPhoto)
    {
        $this->repository->save($sliderPhoto);
    }

    /**
     * {@inheritdoc}
     */
    public function findSliderPhotoBy(array $criteria)
    {
        $sliderPhoto = $this->repository->by($criteria);
        return $sliderPhoto;
    }

    /**
     * {@inheritdoc}
     */
    public function findAllSlides($public = true)
    {
        return $this->repository->all($public);
    }

}