<?php

namespace Mosaic\CMSBundle\Doctrine;

use Cocur\Slugify\Slugify;
use Cocur\Slugify\SlugifyInterface;
use Mosaic\CMSBundle\Model\LocaleManagerInterface;
use Mosaic\CMSBundle\Model\NewsManager as BaseNewsManager;
use Mosaic\CMSBundle\Repository\NewsRepositoryInterface;
use Mosaic\CMSBundle\Model\NewsInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mosaic\CMSBundle\Service\NewsTranslationsGet;

class NewsManager extends BaseNewsManager
{

    /**
     * @var string
     */
    protected $class;
    /**
     * @var NewsRepositoryInterface
     */
    protected $repository;
    /**
     * @var Slugify
     */
    protected $slugger;
    /**
     * @var LocaleManagerInterface
     */
    protected $localeManager;
    /**
     * @var NewsTranslationsGet
     */
    protected $newsTranslationGet;

    /**
     * NewsManager constructor.
     * @param NewsRepositoryInterface $newsRepository
     * @param ObjectManager $objectManager
     * @param string $class
     * @param SlugifyInterface $slugger
     * @param LocaleManagerInterface $localeManager
     * @param NewsTranslationsGet $newsTranslationGet
     */
    public function __construct(
        NewsRepositoryInterface $newsRepository,
        ObjectManager $objectManager,
        $class,
        SlugifyInterface $slugger,
        LocaleManagerInterface $localeManager,
        NewsTranslationsGet $newsTranslationGet
    ) {

        $this->repository = $newsRepository;

        $metaData = $objectManager->getClassMetadata($class);
        $this->class = $metaData->getName();

        $this->slugger = $slugger;
        $this->localeManager = $localeManager;
        $this->newsTranslationGet = $newsTranslationGet;
    }

    /**
     * Deletes a news.
     *
     * @param NewsInterface $news
     *
     * @return void
     */
    public function deleteNews(NewsInterface $news)
    {
        $this->repository->delete($news);
    }

    /**
     * Returns the news's fully qualified class name.
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Updates a news.
     *
     * @param NewsInterface $news
     *
     * @return void
     */
    public function updateNews(NewsInterface $news)
    {
        $news->setUpdateDate();
        $this->repository->save($news);

        $locales = $this->localeManager->getEnabledLocales();
        foreach ($locales AS $locale) {
            $news = $this->newsTranslationGet->byLocale($news, $locale->getCode());
            $news = $this->normalizeSlug($news);
            $this->repository->save($news);
        }
    }

    /**
     * Finds one news by the given criteria.
     *
     * @param array $criteria
     *
     * @return NewsInterface or null
     */
    public function findNewsBy(array $criteria)
    {
        return $this->repository->by($criteria);
    }

    /**
     * Returns a collection with all news instances.
     * @param bool $published if true return public news only, false ignores that field
     *
     * @return \Traversable
     */
    public function findAllNews($published = true)
    {
        return $this->repository->all($published);
    }

    /**
     * @param NewsInterface $news
     * @return NewsInterface
     */
    public function normalizeSlug(NewsInterface $news)
    {
        $toSlug = $news->getSlug() ? $news->getSlug() : $news->getTitle();
        $normalizedSlug = $this->slugger->slugify($toSlug);
        $news->setSlug($normalizedSlug);
        return $news;
    }
}