<?php

namespace Mosaic\CMSBundle\Service;

use Mosaic\CMSBundle\Model\GalleryInterface;
use Mosaic\CMSBundle\Repository\GalleryRepositoryInterface;
use Psr\Log\InvalidArgumentException;

class GalleryCreate
{
    /**
     * @var GalleryRepositoryInterface
     */
    private $galleryRepository;

    /**
     * @var string
     */
    private $galleryClass;

    /**
     * GalleryCreate constructor.
     * @param GalleryRepositoryInterface $galleryRepository
     * @param $galleryClass
     */
    public function __construct(GalleryRepositoryInterface $galleryRepository, $galleryClass)
    {
        $this->galleryRepository = $galleryRepository;
        $this->galleryClass = $galleryClass;
    }

    /**
     * @param $machineName
     * @return GalleryInterface
     */
    public function fromMachineName($machineName)
    {
        if($this->machineNameExists($machineName)) {
            throw new InvalidArgumentException('Ja hi ha una galeria amb aquest nom, si us plau trieu un altre.');
        }

        $gallery = new $this->galleryClass($machineName);
        
        $this->galleryRepository->save($gallery);

        return $gallery;
    }

    /**
     * @param $machineName
     * @return bool
     */
    public function machineNameExists($machineName)
    {
        return $this->galleryRepository->by(['machineName' => $machineName]) instanceof $this->galleryClass;
    }

}