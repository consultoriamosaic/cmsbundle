<?php

namespace Mosaic\CMSBundle\Service;

use Doctrine\ORM\EntityManager;
use Mosaic\CMSBundle\Form\DTO\SliderPhotoDescriptionDTO;
use Mosaic\CMSBundle\Model\SliderPhoto;

class SliderTranslationsGet
{

    private $translationRepository;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * SliderTranslationsGet constructor.
     * @param $translationRepository
     * @param EntityManager $entityManager
     */
    public function __construct($translationRepository, EntityManager $entityManager)
    {
        $this->translationRepository = $translationRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param SliderPhoto $sliderPhoto
     * @param string $localeCode
     * @return SliderPhoto
     */
    public function byLocale(SliderPhoto $sliderPhoto, $localeCode)
    {
        $sliderPhoto->setLocale($localeCode);
        $this->entityManager->refresh($sliderPhoto);
        return $sliderPhoto;
    }

    /**
     * @param SliderPhoto $sliderPhoto
     * @return array of SliderPhotoDescriptionDTO
     */
    public function all(SliderPhoto $sliderPhoto)
    {
        return $this->translationRepository->findTranslations($sliderPhoto);
    }
}