<?php

namespace Mosaic\CMSBundle\Service;

use Mosaic\CMSBundle\Repository\NewsRepositoryInterface;

class NewsPaginator
{

    /**
     * @var NewsRepositoryInterface
     */
    private $newsRepository;

    /**
     * @var int
     */
    private $newsPerPage;

    /**
     * NewsPaginator constructor.
     * @param NewsRepositoryInterface $newsRepository
     */
    public function __construct(NewsRepositoryInterface $newsRepository, $newsPerPage)
    {
        $this->newsRepository = $newsRepository;
        $this->newsPerPage = $newsPerPage;
    }

    /**
     * @param int $page
     * @param int $newsPerPage
     * @return array (totalNews, NewsPerPage, totalPages, currentPage, news)
     */
    public function __invoke($page, $newsPerPage = 0)
    {

        if (0 < $newsPerPage) {
            $this->newsPerPage = $newsPerPage;
        }

        if(0 >= $page) {
            $page = 1;
        }

        $offset = ($page - 1) * $newsPerPage;
        $totalNews = $this->newsRepository->count();

        return [
            'totalNews'   => $totalNews,
            'newsPerPage' => $this->newsPerPage,
            'totalPages'  => (int)ceil($totalNews / $this->newsPerPage),
            'currentPage' => $page,
            'news'        => $this->newsRepository->page($this->newsPerPage, $offset)
        ];
    }

}