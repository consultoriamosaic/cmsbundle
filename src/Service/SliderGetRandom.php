<?php
namespace Mosaic\CMSBundle\Service;

use Mosaic\CMSBundle\Model\SliderPhotoInterface;
use Mosaic\CMSBundle\Repository\SliderRepositoryInterface;


class SliderGetRandom
{

    /**
     * @var SliderRepositoryInterface
     */
    private $sliderRepository;

    public function __construct(SliderRepositoryInterface $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * @return null|SliderPhotoInterface
     */
    public function __invoke()
    {
        $slides = $this->sliderRepository->all();

        $totalSlides = count($slides);

        if (0 == $totalSlides) {
            return null;
        }

        return $slides[rand(0, $totalSlides - 1)];
    }

}