<?php

namespace Mosaic\CMSBundle\Service;

use Doctrine\ORM\EntityManager;
use Mosaic\CMSBundle\Model\PageInterface;

class PageTranslationsGet
{

    private $translationRepository;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * SliderTranslationsGet constructor.
     * @param $translationRepository
     * @param EntityManager $entityManager
     */
    public function __construct($translationRepository, EntityManager $entityManager)
    {
        $this->translationRepository = $translationRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param PageInterface $page
     * @param string $localeCode
     * @return PageInterface
     */
    public function byLocale(PageInterface $page, $localeCode)
    {
        $page->setLocale($localeCode);
        $this->entityManager->refresh($page);
        return $page;
    }

    /**
     * @param PageInterface $page
     * @return array of translations
     */
    public function all(PageInterface $page)
    {
        return $this->translationRepository->findTranslations($page);
    }

}