<?php

namespace Mosaic\CMSBundle\Service;

use Doctrine\ORM\EntityManager;
use Mosaic\CMSBundle\Model\NewsInterface;

class NewsTranslationsGet
{
    private $translationRepository;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * NewsTranslationsGet constructor.
     * @param $translationRepository
     * @param EntityManager $entityManager
     */
    public function __construct($translationRepository, EntityManager $entityManager)
    {
        $this->translationRepository = $translationRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param NewsInterface $news
     * @param string $localeCode
     * @return NewsInterface
     */
    public function byLocale(NewsInterface $news, $localeCode)
    {
        $news->setLocale($localeCode);
        $this->entityManager->refresh($news);
        return $news;
    }

    /**
     * @param NewsInterface $news
     * @return array of translations
     */
    public function all(NewsInterface $news)
    {
        return $this->translationRepository->findTranslations($news);
    }
}