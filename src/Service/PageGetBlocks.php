<?php

namespace Mosaic\CMSBundle\Service;

use Mosaic\CMSBundle\Model\PageManagerInterface;

class PageGetBlocks
{
    /**
     * @var PageManagerInterface
     */
    private $pageManager;

    /**
     * PageGetBlocks constructor.
     * @param PageManagerInterface $pageManager
     */
    public function __construct(PageManagerInterface $pageManager)
    {
        $this->pageManager = $pageManager;
    }

    /**
     * @param array $blocksToRetrieve
     * @return array
     */
    public function __invoke(array $blocksToRetrieve)
    {
        $blocks = [];

        foreach ($blocksToRetrieve AS $blockMachineName) {
            $blocks[$blockMachineName] = $this->pageManager->findPageByMachineName($blockMachineName);
        }

        return $blocks;
    }

}