<?php

namespace Mosaic\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Mosaic\CMSBundle\Model\NewsInterface;

class DoctrineNewsRepository extends EntityRepository implements NewsRepositoryInterface
{

    private $class;

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(NewsInterface $news)
    {
        $this->_em->remove($news);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function save(NewsInterface $news)
    {
        $this->_em->persist($news);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function by(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function all($published = true)
    {
        $criteria = $published ? ['published' => true] : [];
        $order = ['creationDate' => 'DESC'];

        return $this->findBy($criteria, $order);
    }

    /**
     * {@inheritdoc}
     */
    public function count($published = true)
    {
        $dql = $this->_em->createQueryBuilder();
        $dql->select('COUNT(n.id)')
            ->from($this->class, 'n')
            ->where('n.published = :published')
            ->setParameter('published', $published);

        return $dql->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritdoc}
     */
    public function page($limit, $offset, $criteria = ['published' => true], $orderBy = ['creationDate' => 'DESC'])
    {
        return $this->findBy($criteria, $orderBy, $limit, $offset);
    }
}