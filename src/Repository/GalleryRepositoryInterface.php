<?php

namespace Mosaic\CMSBundle\Repository;

use Mosaic\CMSBundle\Model\GalleryInterface;

interface GalleryRepositoryInterface
{
    /**
     * @param GalleryInterface $gallery
     * @return void
     */
    public function delete(GalleryInterface $gallery);

    /**
     * @param GalleryInterface $gallery
     * @return void
     */
    public function save(GalleryInterface $gallery);

    /**
     * @param array $criteria
     * @return GalleryInterface
     */
    public function by(array $criteria);

    /**
     * @param bool $public
     * @return GalleryInterface[]
     */
    public function all($public = true);
}