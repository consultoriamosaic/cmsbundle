<?php

namespace Mosaic\CMSBundle\Repository;

use Mosaic\CMSBundle\Model\NewsInterface;

interface NewsRepositoryInterface
{
    /**
     * @param string $class
     */
    public function setClass($class);

    /**
     * @param NewsInterface $news
     * @return void
     */
    public function delete(NewsInterface $news);

    /**
     * @param NewsInterface $news
     * @return void
     */
    public function save(NewsInterface $news);

    /**
     * @param array $criteria
     * @return NewsInterface
     */
    public function by(array $criteria);

    /**
     * @param bool $published
     * @return \Traversable
     */
    public function all($published = true);

    /**
     * @param bool $published
     * @return int
     */
    public function count($published = true);

    /**
     * @param int $limit
     * @param int $offset
     * @param array $criteria
     * @param array $orderBy
     * @return \Traversable
     */
    public function page($limit, $offset, $criteria = ['published' => true], $orderBy = ['creationDate' => 'DESC']);

}