<?php

namespace Mosaic\CMSBundle\Repository;

use Mosaic\CMSBundle\Model\SliderPhotoInterface;

interface SliderRepositoryInterface
{
    /**
     * @param SliderPhotoInterface $sliderPhoto
     * @return void
     */
    public function delete(SliderPhotoInterface $sliderPhoto);

    /**
     * @param SliderPhotoInterface $sliderPhoto
     * @return void
     */
    public function save(SliderPhotoInterface $sliderPhoto);

    /**
     * @param array $criteria
     * @return SliderPhotoInterface
     */
    public function by(array $criteria);

    /**
     * @param bool $public
     * @return \Traversable
     */
    public function all($public = true);

    /**
     * @param string $machineName
     * @return SliderPhotoInterface[]
     */
    public function gallery($machineName);
}