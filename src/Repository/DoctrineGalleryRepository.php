<?php

namespace Mosaic\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Mosaic\CMSBundle\Model\GalleryInterface;

class DoctrineGalleryRepository extends EntityRepository implements GalleryRepositoryInterface
{

    /**
     * @param GalleryInterface $gallery
     * @return void
     */
    public function delete(GalleryInterface $gallery)
    {
        $this->_em->remove($gallery);
        $this->_em->flush();
    }

    /**
     * @param GalleryInterface $gallery
     * @return void
     */
    public function save(GalleryInterface $gallery)
    {
        $this->_em->persist($gallery);
        $this->_em->flush();
    }

    /**
     * @param array $criteria
     * @return GalleryInterface
     */
    public function by(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * @param bool $public
     * @return GalleryInterface[]
     */
    public function all($public = true)
    {
        $criteria = $public ? ['enabled' => true] : [];
        $order = ['machineName' => 'ASC'];

        return $this->findBy($criteria, $order);
    }
}