<?php

namespace Mosaic\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Mosaic\CMSBundle\Model\SliderPhotoInterface;

class DoctrineSliderRepository extends EntityRepository implements SliderRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function delete(SliderPhotoInterface $sliderPhoto)
    {
        $this->_em->remove($sliderPhoto);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function save(SliderPhotoInterface $sliderPhoto)
    {
        $this->_em->persist($sliderPhoto);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function by(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function all($public = true)
    {
        $criteria = $public ? ['public' => true] : [];
        $order = ['position' => 'ASC'];
        
        return $this->findBy($criteria, $order);
    }

    /**
     * {@inheritdoc}
     */
    public function gallery($machineName)
    {
        $queryBuilder = $this->createQueryBuilder('s');

        $queryBuilder
            ->innerJoin('s.gallery', 'g')
            ->where('g.machineName = :machineName')
            ->andWhere('s.public= true')
            ->orderBy("s.position", "asc")
            ->setParameter('machineName', $machineName);

        return $queryBuilder->getQuery()->getResult();
    }

}