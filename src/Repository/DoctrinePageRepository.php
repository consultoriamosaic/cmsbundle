<?php

namespace Mosaic\CMSBundle\Repository;

use Mosaic\CMSBundle\Model\PageInterface;
use Doctrine\ORM\EntityRepository;

class DoctrinePageRepository extends EntityRepository implements PageRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function delete(PageInterface $page)
    {
        $this->_em->remove($page);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function save(PageInterface $page)
    {
        $this->_em->persist($page);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function by(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        $order = ['machineName' => 'ASC'];
        return $this->findBy([], $order);
    }

    /**
     * @param string $machineNamePrefix
     * @return PageInterface|null
     */
    public function getPage($machineNamePrefix)
    {
        $dql = $this->createQueryBuilder("p");
        $dql
            ->where('p.public = true')
            ->andWhere('p.machineName = :page_prefix')
            ->setParameter('page_prefix', 'page_' . $machineNamePrefix);

        $query = $dql->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param string $machineNamePrefix
     * @return PageInterface[]
     */
    public function getBlocks($machineNamePrefix)
    {
        $dql = $this->createQueryBuilder("p");
        $dql
            ->where('p.public = true')
            ->andWhere('p.machineName LIKE :block_prefix')
            ->setParameter('block_prefix', 'block_' . $machineNamePrefix . '%');

        $query = $dql->getQuery();

        return $query->getResult();
    }
}