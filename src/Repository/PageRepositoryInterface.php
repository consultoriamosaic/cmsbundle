<?php

namespace Mosaic\CMSBundle\Repository;

use Mosaic\CMSBundle\Model\PageInterface;

interface PageRepositoryInterface
{
    /**
     * @param PageInterface $page
     * @return void
     */
    public function delete(PageInterface $page);

    /**
     * @param PageInterface $page
     * @return void
     */
    public function save(PageInterface $page);

    /**
     * @param array $criteria
     * @return PageInterface
     */
    public function by(array $criteria);

    /**
     * @return \Traversable
     */
    public function all();

    /**
     * @param string $machineNamePrefix
     * @return PageInterface|null
     */
    public function getPage($machineNamePrefix);

    /**
     * @param string $machineNamePrefix
     * @return PageInterface[]
     */
    public function getBlocks($machineNamePrefix);

}