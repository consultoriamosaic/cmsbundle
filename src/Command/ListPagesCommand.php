<?php

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class ListPagesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:page:list')
            ->setDescription('Lists all pages.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $manipulator = $this->getContainer()->get('mosaic_cms.page_manipulator');

        $pages = $manipulator->getAll();

        if (0 == count($pages)) {
            $output->writeln('No pages found.');
        } else {
            $table = new Table($output);
            $table->setHeaders(array('Title', 'Machine name', 'Public'));

            foreach ($pages AS $page) {
                $table->addRow(array($page->getTitle(), $page->getMachineName(), $page->isPublic() ? 'Yes' : 'No'));
            }

            $table->render();
        }

    }

}