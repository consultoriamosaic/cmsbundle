<?php

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;

class DeletePageCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:page:delete')
            ->setDescription('Deletes a page.')
            ->setDefinition(array(
                new InputArgument('machineName', InputArgument::REQUIRED, 'The machineName')
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $machineName = $input->getArgument('machineName');

        $manipulator = $this->getContainer()->get('mosaic_cms.page_manipulator');

        $success = $manipulator->delete($machineName);

        $output->writeln($success ? 'Page deleted.' : '<error>Page not found.</error>');

    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        /*if (!$this->getHelperSet()->has('question')) {
            $this->legacyInteract($input, $output);

            return;
        }*/

        $questions = array();

        if (!$input->getArgument('machineName')) {
            $question = new Question('Enter the machine name of the page to delete:');
            $question->setValidator(function ($machineName) {
                if (empty($machineName)) {
                    throw new \Exception('No machine name entered.');
                }

                return $machineName;
            });
            $questions['machineName'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}