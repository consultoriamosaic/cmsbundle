<?php

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use Mosaic\CMSBundle\Model\LocaleInterface;

class ListLocalesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:locale:list')
            ->setDescription('Lists all locales.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $manipulator = $this->getContainer()->get('mosaic_cms.locale_manipulator');

        $locales = $manipulator->getAll();

        if (0 == count($locales)) {
            $output->writeln('No locales found.');
        } else {
            $table = new Table($output);
            $table->setHeaders(array('Code', 'Enabled'));

            foreach ($locales AS $locale) {
                $table->addRow(array($locale->getCode(), $locale->isEnabled() ? 'Yes' : 'No'));
            }

            $table->render();
        }

    }
}
