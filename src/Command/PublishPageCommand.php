<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 9/05/16
 * Time: 17:34
 */

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;

class PublishPageCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:page:publish')
            ->setDescription('Set public state of a page.')
            ->setDefinition(array(
                new InputArgument('machineName', InputArgument::REQUIRED, 'The machineName'),
                new InputArgument('public', InputArgument::REQUIRED, 'Public or not')
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $machineName = $input->getArgument('machineName');
        $public = $input->getArgument('public') == 'Yes';

        $manipulator = $this->getContainer()->get('mosaic_cms.page_manipulator');

        $success = $manipulator->publish($machineName, $public);

        $pageStatus = $public ? 'published' : 'unpublished';
        $output->writeln($success ? 'Page ' . $pageStatus . '.' : '<error>Page not found.</error>');

    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        /*if (!$this->getHelperSet()->has('question')) {
            $this->legacyInteract($input, $output);

            return;
        }*/

        $questions = array();

        if (!$input->getArgument('machineName')) {
            $question = new Question('Enter the machineName of the page to un/publish:');
            $question->setValidator(function ($machineName) {
                if (empty($machineName)) {
                    throw new \Exception('No machineName entered.');
                }

                return $machineName;
            });
            $questions['machineName'] = $question;
        }

        if (!$input->getArgument('public')) {
            $question = new ChoiceQuestion(
                'Make it public?',
                array('No', 'Yes'),
                0
            );
            $questions['public'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}