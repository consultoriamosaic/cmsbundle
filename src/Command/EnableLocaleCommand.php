<?php

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class EnableLocaleCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:locale:enable')
            ->setDescription('Set enabled state of a locale.')
            ->setDefinition(array(
                new InputArgument('code', InputArgument::REQUIRED, 'The code'),
                new InputArgument('enabled', InputArgument::REQUIRED, 'Enabled or not')
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $code = $input->getArgument('code');
        $enabled = $input->getArgument('enabled') == 'Yes';

        $manipulator = $this->getContainer()->get('mosaic_cms.locale_manipulator');

        $success = $manipulator->enable($code, $enabled);

        $localeStatus = $enabled ? 'enabled' : 'disabled';
        $output->writeln($success ? 'Locale ' . $localeStatus . '.' : '<error>Locale not found.</error>');

    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();

        if (!$input->getArgument('code')) {
            $question = new Question('Enter the code of the locale to enable/disable:');
            $question->setValidator(function ($slug) {
                if (empty($slug)) {
                    throw new \Exception('No code entered.');
                }

                return $slug;
            });
            $questions['code'] = $question;
        }

        if (!$input->getArgument('enabled')) {
            $question = new ChoiceQuestion(
                'Enable it?',
                array('No', 'Yes')
            );
            $questions['enabled'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}