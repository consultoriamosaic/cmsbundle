<?php

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;

class DeleteLocaleCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:locale:delete')
            ->setDescription('Deletes a locale.')
            ->setDefinition(array(
                new InputArgument('code', InputArgument::REQUIRED, 'The code')
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $code = $input->getArgument('code');

        $manipulator = $this->getContainer()->get('mosaic_cms.locale_manipulator');

        $success = $manipulator->delete($code);

        $output->writeln($success ? 'Locale deleted.' : '<error>Locale not found.</error>');

    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {

        $questions = array();

        if (!$input->getArgument('code')) {
            $question = new Question('Enter the code of the locale to delete:');
            $question->setValidator(function ($code) {
                if (empty($code)) {
                    throw new \Exception('No code entered.');
                }

                return $code;
            });
            $questions['code'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
