<?php

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ChoiceQuestion;


class CreateLocaleCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:locale:create')
            ->setDescription('Creates a locale.')
            ->setDefinition(array(
                new InputArgument('code', InputArgument::REQUIRED, 'The code'),
                new InputArgument('enabled', InputArgument::REQUIRED, 'Enabled')
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $code = $input->getArgument('code');
        $enabled = $input->getArgument('enabled') == 'Yes';

        $manipulator = $this->getContainer()->get('mosaic_cms.locale_manipulator');

        if (!$manipulator->checkLocaleExists($code)) {
            $manipulator->create($code, $enabled);
            $output->writeln('Locale created.');
        } else {
            $output->writeln('<error>There\'s already a Locale with this code</error>');
        }

    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {

        $questions = array();

        if (!$input->getArgument('code')) {
            $question = new Question('Please insert the code:');
            $question->setValidator(function ($code) {
                if (empty($code)) {
                    throw new \Exception('Code must be filled.');
                }

                return $code;
            });
            $questions['code'] = $question;
        }

        if (!$input->getArgument('enabled')) {
            $question = new ChoiceQuestion(
                'Enable it?',
                array('No', 'Yes')
            );
            $questions['enabled'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
