<?php

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;

class CreatePageCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:page:create')
            ->setDescription('Creates a page.')
            ->setDefinition(array(
                new InputArgument('title', InputArgument::REQUIRED, 'The title'),
                new InputArgument('machineName', InputArgument::REQUIRED, 'The machineName')
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $title = $input->getArgument('title');
        $machineName = $input->getArgument('machineName');

        $manipulator = $this->getContainer()->get('mosaic_cms.page_manipulator');
        $manipulator->create($title, $machineName);

        $output->writeln('Page created.');

    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {

        $questions = array();

        if (!$input->getArgument('title')) {
            $question = new Question('Please choose a title:');
            $question->setValidator(function($title) {
                if (empty($title)) {
                    throw new \Exception('Title should be filled.');
                }

                return $title;
            });
            $questions['title'] = $question;
        }

        if (!$input->getArgument('machineName')) {
            $question = new Question('Set up the machineName:');
            $question->setValidator(function($machineName) {
                if (empty($machineName)) {
                    throw new \Exception('Machine name must be filled.');
                }

                return $machineName;
            });
            $questions['machineName'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
