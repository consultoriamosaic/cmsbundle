<?php

namespace Mosaic\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Yaml\Yaml;

class PreloadPagesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mosaic:cms:page:preload')
            ->setDescription('Loads a list of pages from a YAML.')
            ->setDefinition(array(
                new InputArgument('filepath', InputArgument::REQUIRED, 'The file'),
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filepath = $input->getArgument('filepath');

        $pages = Yaml::parse(file_get_contents($filepath));

        $manipulator = $this->getContainer()->get('mosaic_cms.page_manipulator');

        foreach ($pages AS $machineName => $title) {
            if ($manipulator->checkMachineName($machineName)) {
                $output->writeln('<error>Machine name  ' . $machineName . ' already exists, skiped.</error>');
            } else {
                $manipulator->create($title, $machineName);
                $output->writeln('Page ' . $machineName . ' created.');
            }

        }

    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {

        $questions = array();

        if (!$input->getArgument('filepath')) {
            $question = new Question('Enter the YAML file to load pages from: ');
            $question->setValidator(function ($filepath) {
                if (!is_file($filepath)) {
                    throw new \Exception('File not found.');
                }

                return $filepath;
            });
            $questions['filepath'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
