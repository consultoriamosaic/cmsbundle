<?php

namespace Mosaic\CMSBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Gedmo\Translatable\TranslatableListener;

class DoctrineExtensionListener
{
    /**
     * @var TranslatableListener
     */
    private $translatableListener;

    /**
     * DoctrineExtensionListener constructor.
     * @param TranslatableListener $translatableListener
     */
    public function __construct(TranslatableListener $translatableListener)
    {
        $this->translatableListener = $translatableListener;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onLateKernelRequest(GetResponseEvent $event)
    {
        $this->translatableListener->setTranslatableLocale($event->getRequest()->getLocale());
    }
}