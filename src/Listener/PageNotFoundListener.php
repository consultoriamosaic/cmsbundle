<?php

namespace Mosaic\CMSBundle\Listener;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageNotFoundListener
{
    public function onMosaiccmsPageNotfound()
    {
        throw new NotFoundHttpException();
    }
}