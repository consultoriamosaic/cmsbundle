<?php

namespace Mosaic\CMSBundle\Form\Factory;

interface FactoryInterface
{
    /**
     * @param mixed $data The initial data
     * @param array $options The options
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createForm($data = null, $options = array());
}