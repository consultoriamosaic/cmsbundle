<?php

namespace Mosaic\CMSBundle\Form\Type;

use Mosaic\CMSBundle\Form\DTO\PageDTO;
use Mosaic\CMSBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('machine_name',
                LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                    'label'              => 'form.machineName',
                    'translation_domain' => 'MosaicCMSBundle',
                    'required'           => true,
                    'property_path'      => 'machineName'
                ])
            ->add('public', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\CheckboxType'), [
                'label'              => 'form.public',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mosaic\CMSBundle\Form\DTO\PageDTO',
            'empty_data' => function (FormInterface $form) {
                return new PageDTO(
                    $form->get('machine_name')->getData(),
                    $form->get('public')->getData()
                );
            }
        ));
    }

    // BC for SF < 2.7
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    // BC for SF < 3.0
    public function getName()
    {
        return 'mosaic_cms_page';
    }
}