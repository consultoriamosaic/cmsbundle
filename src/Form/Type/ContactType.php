<?php

namespace Mosaic\CMSBundle\Form\Type;

use Mosaic\CMSBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;
use Mosaic\CMSBundle\Form\DTO\ContactDTO;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
            'label'        => 'form.name',
            'translation_domain' => 'MosaicCMSBundle'
        ]);
        $builder->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), [
            'label'        => 'form.email',
            'translation_domain' => 'MosaicCMSBundle'
        ]);
        $builder->add('message', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextareaType'), [
            'label'        => 'form.message',
            'translation_domain' => 'MosaicCMSBundle'
        ]);
        $builder->add('conditions', CheckboxType::class, [
            'label'              => 'form.conditions',
            'translation_domain' => 'MosaicCMSBundle',
            'required'           => true
        ]);
        $builder->add('send', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', [
            'label' => 'form.send'
        ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => 'Mosaic\CMSBundle\Form\DTO\ContactDTO',
            'empty_data'    => function (FormInterface $form) {
                return new ContactDTO(
                    $form->get('name')->getData(),
                    $form->get('email')->getData(),
                    $form->get('message')->getData(),
                    $form->get('conditions')->getData()
                );
            },
            'csrf_token_id' => 'contact',
            'intention'     => 'contact'
        ]);
    }


    // BC for SF < 2.7
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    // BC for SF < 3.0
    public function getName()
    {
        return 'mosaic_cms_contact';
    }
}
