<?php

namespace Mosaic\CMSBundle\Form\Type;

use Mosaic\CMSBundle\Form\DTO\SliderPhotoDTO;
use Mosaic\CMSBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SliderPhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $object = $builder->getData();
        $new = !($object and !empty($object->id()));

        $builder
            ->add('position', NumberType::class, [
                'label'              => 'form.order',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ])
            ->add('photo', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\FileType'), [
                'label'              => 'form.photo',
                'translation_domain' => 'MosaicCMSBundle',
                'data_class'         => null,
                'required'           => false
            ])
            ->add('public', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\CheckboxType'), [
                'label'              => 'form.public',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ])
            ->add('send', SubmitType::class, [
                    'label'              => ($new ? 'form.create' : 'form.edit'),
                    'translation_domain' => 'MosaicCMSBundle'
                ]

            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mosaic\CMSBundle\Form\DTO\SliderPhotoDTO',
            'empty_data' => function (FormInterface $form) {
                return new SliderPhotoDTO(
                    $form->get('position')->getData(),
                    $form->get('photo')->getData(),
                    $form->get('public')->getData()
                );
            }
        ));
    }

    // BC for SF < 2.7
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    // BC for SF < 3.0
    public function getName()
    {
        return 'mosaic_cms_slider_photo';
    }
}