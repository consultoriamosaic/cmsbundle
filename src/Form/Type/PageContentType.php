<?php

namespace Mosaic\CMSBundle\Form\Type;

use Mosaic\CMSBundle\Form\DTO\PageContentDTO;
use Mosaic\CMSBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                'label'              => 'form.title',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => true
            ])
            ->add('slug', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                'label'              => 'form.slug',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ])
            ->add('shortTitle', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                'label'              => 'form.shortTitle',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ])
            ->add('content', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextareaType'), [
                'label'              => 'form.content',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ])
            ->add('metaTitle', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                    'label'              => 'form.metaTitle',
                    'translation_domain' => 'MosaicCMSBundle',
                    'required'           => false
                ]
            )
            ->add('metaDescription',
                LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextareaType'), [
                    'label'              => 'form.metaDescription',
                    'translation_domain' => 'MosaicCMSBundle',
                    'required'           => false
                ])
            ->add('metaKeywords', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                'label'              => 'form.metaKeywords',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mosaic\CMSBundle\Form\DTO\PageContentDTO',
            'empty_data' => function (FormInterface $form) {
                return new PageContentDTO(
                    $form->get('locale')->getData(),
                    $form->get('page')->getData(),
                    $form->get('title')->getData(),
                    $form->get('slug')->getData(),
                    $form->get('shortTitle')->getData(),
                    $form->get('content')->getData(),
                    $form->get('metaTitle')->getData(),
                    $form->get('metaDescription')->getData(),
                    $form->get('metaKeywords')->getData()
                );
            }
        ));
    }

    // BC for SF < 2.7
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    // BC for SF < 3.0
    public function getName()
    {
        return 'mosaic_cms_page_content';
    }
}