<?php

namespace Mosaic\CMSBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class GalleryCreationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('machine_name', TextType::class, array(
            'constraints'        => array(
                new Assert\NotBlank(),
                new Assert\Length(array('min' => 3, 'max' => 254))
            )
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getName()
    {
        return 'mosaic_cms_gallery_creation';
    }
}