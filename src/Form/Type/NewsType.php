<?php

namespace Mosaic\CMSBundle\Form\Type;

use Mosaic\CMSBundle\Form\DTO\NewsDTO;
use Mosaic\CMSBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('picture', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\FileType'), [
            'data_class'         => null,
            'label'              => 'form.photo',
            'translation_domain' => 'MosaicCMSBundle',
            'required'           => false
        ])
            ->add('published', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\CheckboxType'), [
                'label'              => 'form.published',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mosaic\CMSBundle\Form\DTO\NewsDTO',
            'empty_data' => function (FormInterface $form) {
                return new NewsDTO(
                    $form->get('picture')->getData(),
                    $form->get('published')->getData()
                );
            }
        ));
    }

    // BC for SF < 2.7
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    // BC for SF < 3.0
    public function getName()
    {
        return 'mosaic_cms_news';
    }
}