<?php

namespace Mosaic\CMSBundle\Form\Type;

use Mosaic\CMSBundle\Form\DTO\SliderPhotoDescriptionDTO;
use Mosaic\CMSBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SliderPhotoDescriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                'label'              => 'form.title',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ])
            ->add('alt', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                'label'              => 'form.alt',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ])
            ->add('description', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextareaType'), [
                'label'              => 'form.description',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false
            ])
            ->add('link', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\TextType'), [
                'label'              => 'form.link',
                'translation_domain' => 'MosaicCMSBundle',
                'required'           => false,
                'attr'               => [
                    'placeholder' => 'http://www.example.com'
                ]
            ])
            ->add('send', SubmitType::class, [
                'label'              => 'form.save',
                'translation_domain' => 'MosaicCMSBundle'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mosaic\CMSBundle\Form\DTO\SliderPhotoDescriptionDTO',
            'emtpy_data' => function (FormInterface $form) {
                return new SliderPhotoDescriptionDTO(
                    $form->get('locale')->getData(),
                    $form->get('photo')->getData(),
                    $form->get('title')->getData(),
                    $form->get('alt')->getData(),
                    $form->get('description')->getData(),
                    $form->get('link')->getData()
                );
            }
        ));
    }

    // BC for SF < 2.7
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    // BC for SF < 3.0
    public function getName()
    {
        return 'mosaic_cms_slider_photo_description';
    }
}