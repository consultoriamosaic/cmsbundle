<?php

namespace Mosaic\CMSBundle\Form\DTO;

class ContactDTO
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $message;
    /**
     * @var boolean
     */
    private $conditions;

    /**
     * ContactDTO constructor.
     * @param string $name
     * @param string $email
     * @param string $message
     * @param boolean $conditions
     */
    public function __construct($name, $email, $message, $conditions)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
        $this->conditions = $conditions;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return boolean
     */
    public function isConditions()
    {
        return $this->conditions;
    }

    /**
     * @param boolean $conditions
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
    }
}