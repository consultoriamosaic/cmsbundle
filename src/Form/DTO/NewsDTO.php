<?php


namespace Mosaic\CMSBundle\Form\DTO;

use Mosaic\CMSBundle\Model\NewsInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class NewsDTO
{

    /**
     * @var string
     */
    public $id;
    /**
     * @var mixed
     */
    public $picture;
    /**
     * @var boolean
     */
    public $published;

    /**
     * NewsDTO constructor.
     * @param mixed $picture
     * @param bool $published
     */
    public function __construct($picture, $published)
    {
        $this->picture = $picture;
        $this->published = $published;
    }

    /**
     * @param NewsInterface $news
     * @return NewsDTO
     */
    public static function fromNews(NewsInterface $news)
    {
        $newsDTO = new self(
            $news->getPicture(),
            $news->isPublished()
        );

        $newsDTO->id = $news->getId();

        return $newsDTO;
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function picture()
    {
        return $this->picture;
    }

    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @param boolean $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

}