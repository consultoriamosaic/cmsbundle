<?php

namespace Mosaic\CMSBundle\Form\DTO;

use Mosaic\CMSBundle\Model\PageInterface;

class PageContentDTO
{
    /**
     * @var string
     */
    private $locale;
    /**
     * @var PageInterface
     */
    private $page;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $slug;
    /**
     * @var string
     */
    private $shortTitle;
    /**
     * @var string
     */
    private $content;
    /**
     * @var string
     */
    private $metaTitle;
    /**
     * @var string
     */
    private $metaDescription;
    /**
     * @var string
     */
    private $metaKeywords;

    /**
     * PageContentDTO constructor.
     * @param string $locale
     * @param PageInterface $page
     * @param string $title
     * @param string $slug
     * @param string $shortTitle
     * @param string $content
     * @param string $metaTitle
     * @param string $metaDescription
     * @param string $metaKeywords
     */
    public function __construct(
        $locale,
        PageInterface $page,
        $title,
        $slug,
        $shortTitle,
        $content,
        $metaTitle,
        $metaDescription,
        $metaKeywords
    ) {
        $this->locale = $locale;
        $this->page = $page;
        $this->title = $title;
        $this->slug = $slug;
        $this->shortTitle = $shortTitle;
        $this->content = $content;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @param PageInterface $page
     * @return PageContentDTO
     */
    public static function fromPage(PageInterface $page)
    {
        $pageContentDTO = new self(
            $page->getLocale(),
            $page,
            $page->getTitle(),
            $page->getSlug(),
            $page->getShortTitle(),
            $page->getContent(),
            $page->getMetaTitle(),
            $page->getMetaDescription(),
            $page->getMetaKeywords()
        );

        return $pageContentDTO;
    }

    /**
     * @return string
     */
    public function locale()
    {
        return $this->locale;
    }

    /**
     * @return PageInterface
     */
    public function page()
    {
        return $this->page;
    }

    /**
     * @return string
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function slug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function shortTitle()
    {
        return $this->shortTitle;
    }

    /**
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function metaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @return string
     */
    public function metaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @return string
     */
    public function metaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param string $shortTitle
     */
    public function setShortTitle($shortTitle)
    {
        $this->shortTitle = $shortTitle;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param string $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

}