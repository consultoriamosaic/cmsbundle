<?php

namespace Mosaic\CMSBundle\Form\DTO;

use Mosaic\CMSBundle\Model\SliderPhoto;
use Mosaic\CMSBundle\Model\SliderPhotoInterface;

class SliderPhotoDescriptionDTO
{
    /**
     * @var string
     */
    private $locale;

    /**
     * @var SliderPhoto
     */
    private $sliderPhoto;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $alt;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $link;

    /**
     * SliderPhotoDescriptionDTO constructor.
     * @param string $locale
     * @param string $sliderPhoto
     * @param string $title
     * @param string $alt
     * @param string $description
     * @param string $link
     */
    public function __construct($locale, $sliderPhoto, $title, $alt, $description, $link)
    {
        $this->locale = $locale;
        $this->sliderPhoto = $sliderPhoto;
        $this->title = $title;
        $this->alt = $alt;
        $this->description = $description;
        $this->link = $link;
    }

    /**
     * @param SliderPhotoInterface $sliderPhoto
     * @return SliderPhotoDTO
     */
    public static function fromSliderPhoto(SliderPhotoInterface $sliderPhoto)
    {
        $sliderPhotoDescriptionDTO = new self(
            $sliderPhoto->getLocale(),
            $sliderPhoto,
            $sliderPhoto->getTitle(),
            $sliderPhoto->getAlt(),
            $sliderPhoto->getDescription(),
            $sliderPhoto->getLink()
        );

        return $sliderPhotoDescriptionDTO;
    }

    /**
     * @return string
     */
    public function locale()
    {
        return $this->locale;
    }

    /**
     * @return SliderPhoto
     */
    public function sliderPhoto()
    {
        return $this->sliderPhoto;
    }

    /**
     * @return string
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function alt()
    {
        return $this->alt;
    }

    /**
     * @return string
     */
    public function description()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function link()
    {
        return $this->link;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

}