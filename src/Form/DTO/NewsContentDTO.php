<?php

namespace Mosaic\CMSBundle\Form\DTO;

use Mosaic\CMSBundle\Model\NewsInterface;

class NewsContentDTO
{
    /**
     * @var string
     */
    private $locale;
    /**
     * @var NewsInterface
     */
    private $news;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $slug;
    /**
     * @var string
     */
    private $shortTitle;
    /**
     * @var string
     */
    private $shortContent;
    /**
     * @var string
     */
    private $content;
    /**
     * @var string
     */
    private $metaTitle;
    /**
     * @var string
     */
    private $metaDescription;
    /**
     * @var string
     */
    private $metaKeywords;

    /**
     * NewsContentDTO constructor.
     * @param string $locale
     * @param NewsInterface $news
     * @param string $title
     * @param string $slug
     * @param string $shortTitle
     * @param string $shortContent
     * @param string $content
     * @param string $metaTitle
     * @param string $metaDescription
     * @param string $metaKeywords
     */
    public function __construct(
        $locale,
        NewsInterface $news,
        $title,
        $slug,
        $shortTitle,
        $shortContent,
        $content,
        $metaTitle,
        $metaDescription,
        $metaKeywords
    ) {
        $this->locale = $locale;
        $this->news = $news;
        $this->title = $title;
        $this->slug = $slug;
        $this->shortTitle = $shortTitle;
        $this->content = $content;
        $this->shortContent = $shortContent;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @param NewsInterface $news
     * @return NewsContentDTO
     */
    public static function fromNews(NewsInterface $news)
    {
        $newsContentDTO = new self(
            $news->getLocale(),
            $news,
            $news->getTitle(),
            $news->getSlug(),
            $news->getShortTitle(),
            $news->getShortContent(),
            $news->getContent(),
            $news->getMetaTitle(),
            $news->getMetaDescription(),
            $news->getMetaKeywords()
        );

        return $newsContentDTO;

    }

    /**
     * @return string
     */
    public function locale()
    {
        return $this->locale;
    }

    /**
     * @return NewsInterface
     */
    public function news()
    {
        return $this->news;
    }

    /**
     * @return string
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function slug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function shortTitle()
    {
        return $this->shortTitle;
    }

    /**
     * @return string
     */
    public function shortContent()
    {
        return $this->shortContent;
    }

    /**
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function metaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @return string
     */
    public function metaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @return string
     */
    public function metaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param string $shortTitle
     */
    public function setShortTitle($shortTitle)
    {
        $this->shortTitle = $shortTitle;
    }

    /**
     * @param string $shortContent
     */
    public function setShortContent($shortContent)
    {
        $this->shortContent = $shortContent;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param string $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

}