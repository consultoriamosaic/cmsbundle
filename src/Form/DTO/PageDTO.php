<?php

namespace Mosaic\CMSBundle\Form\DTO;

use Mosaic\CMSBundle\Model\PageInterface;

class PageDTO
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $machineName;
    /**
     * @var boolean
     */
    public $public;

    /**
     * PageDTO constructor.
     * @param string $machineName
     * @param bool $public
     */
    public function __construct($machineName, $public)
    {
        $this->machineName = $machineName;
        $this->public = $public;
    }

    public static function fromPage(PageInterface $page)
    {
        $pageDTO = new self(
            $page->getMachineName(),
            $page->isPublic()
        );

        $pageDTO->id = $page->getId();

        return $pageDTO;
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function machineName()
    {
        return $this->machineName;
    }

    /**
     * @return boolean
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * @param string $machineName
     */
    public function setMachineName($machineName)
    {
        $this->machineName = $machineName;
    }

    /**
     * @param boolean $public
     */
    public function setPublic($public)
    {
        $this->public = $public;
    }
    
}

