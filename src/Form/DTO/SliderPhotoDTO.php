<?php

namespace Mosaic\CMSBundle\Form\DTO;

use Mosaic\CMSBundle\Model\SliderPhotoInterface;

class SliderPhotoDTO
{

    /**
     * @var string
     */
    private $id;

    /**
     * @var int
     */
    private $position;

    /**
     * @var mixed
     */
    private $photo;

    /**
     * @var boolean
     */
    private $public;

    /**
     * SliderPhotoDTO constructor.
     * @param int $position
     * @param mixed $photo
     * @param bool $public
     */
    public function __construct($position, $photo, $public)
    {
        $this->position = $position;
        $this->photo = $photo;
        $this->public = $public;
    }

    /**
     * @param SliderPhotoInterface $sliderPhoto
     * @return SliderPhotoDTO
     */
    public static function fromSliderPhoto(SliderPhotoInterface $sliderPhoto)
    {
        $sliderPhotoDTO = new self(
            $sliderPhoto->getPosition(),
            $sliderPhoto->getPhoto(),
            $sliderPhoto->isPublic()
        );

        $sliderPhotoDTO->id = $sliderPhoto->getId();

        return $sliderPhotoDTO;
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function position()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function photo()
    {
        return $this->photo;
    }

    /**
     * @return boolean
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @param boolean $public
     */
    public function setPublic($public)
    {
        $this->public = $public;
    }

}