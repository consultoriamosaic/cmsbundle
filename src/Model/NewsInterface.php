<?php

namespace Mosaic\CMSBundle\Model;


use Symfony\Component\HttpFoundation\File\UploadedFile;

interface NewsInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getSlug();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getShortTitle();

    /**
     * @return string
     */
    public function getShortContent();

    /**
     * @return string
     */
    public function getContent();

    /**
     * @return string
     */
    public function getMetaTitle();

    /**
     * @return mixed
     */
    public function getPicture();

    /**
     * @return string
     */
    public function getMetaDescription();

    /**
     * @return string
     */
    public function getMetaKeywords();

    /**
     * @return \DateTime
     */
    public function getCreationDate();

    /**
     * @return \DateTime
     */
    public function getUpdateDate();

    /**
     * @return boolean
     */
    public function isPublished();

    /**
     * @return string
     */
    public function getLocale();

    /**
     * @param string $slug
     * @return self
     */
    public function setSlug($slug);

    /**
     * @param string $title
     * @return self
     */
    public function setTitle($title);

    /**
     * @param string $shortContent
     * @return self
     */
    public function setShortContent($shortContent);

    /**
     * @param string $content
     * @return self
     */
    public function setContent($content);

    /**
     * @param string $metaDescription
     * @return self
     */
    public function setMetaDescription($metaDescription);

    /**
     * @param string $metaKeywords
     * @return self
     */
    public function setMetaKeywords($metaKeywords);

    /**
     * @param boolean $published
     * @return self
     */
    public function publish($published);

    /**
     * @return self
     */
    public function setUpdateDate();

    /**
     * Sets short title.
     *
     * @param string $shortTitle
     * @return self
     */
    public function setShortTitle($shortTitle);

    /**
     * Sets meta title.
     *
     * @param string $metaTitle
     * @return self
     */
    public function setMetaTitle($metaTitle);

    /**
     * @param UploadedFile $file
     */
    public function savePicture(UploadedFile $file);

    /**
     * @return void
     */
    public function removePicture();

    /**
     * @return string
     */
    public function getUploadDir();

    /**
     * @param string $locale
     */
    public function setLocale($locale);

}