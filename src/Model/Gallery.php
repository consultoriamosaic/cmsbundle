<?php

namespace Mosaic\CMSBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

abstract class Gallery implements GalleryInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $machineName;

    /**
     * @var boolean
     */
    protected $enabled;

    /**
     * @var ArrayCollection
     */
    protected $sliderPhotos;

    /**
     * Gallery constructor.
     * @param string $machineName
     */
    public function __construct($machineName)
    {
        $this->machineName = $machineName;
        $this->sliderPhotos = new ArrayCollection();
        $this->enabled = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMachineName()
    {
        return $this->machineName;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return ArrayCollection
     */
    public function getSliderPhotos()
    {
        return $this->sliderPhotos;
    }

}