<?php

namespace Mosaic\CMSBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface SliderPhotoInterface
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @return string
     */
    public function getPhoto();

    /**
     * @return int
     */
    public function getPosition();

    /**
     * @return boolean
     */
    public function isPublic();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getLink();

    /**
     * @return string
     */
    public function getAlt();

    /**
     * @return string
     */
    public function getLocale();

    /**
     * @return GalleryInterface
     */
    public function getGallery();

    /**
     * @param int $position
     */
    public function setPosition($position);

    /**
     * @param boolean $public
     */
    public function setPublic($public);

    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @param string $description
     */
    public function setDescription($description);

    /**
     * @param string $link
     */
    public function setLink($link);

    /**
     * @param string $alt
     */
    public function setAlt($alt);

    /**
     * @param string $locale
     */
    public function setLocale($locale);

    /**
     * @param mixed
     */
    public function savePhoto($uploadedFile);

    /**
     * @param GalleryInterface $gallery
     */
    public function setGallery(GalleryInterface $gallery);
}