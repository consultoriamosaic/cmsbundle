<?php

namespace Mosaic\CMSBundle\Model;


use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class News implements NewsInterface
{

    protected $id;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $shortTitle;

    /**
     * @var string
     */
    protected $shortContent;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var string
     */
    protected $metaTitle;

    /**
     * @var string
     */
    protected $picture;

    /**
     * @var string
     */
    protected $metaDescription;

    /**
     * @var string
     */
    protected $metaKeywords;

    /**
     * @var \DateTime
     */
    protected $creationDate;

    /**
     * @var \DateTime
     */
    protected $updateDate;

    /**
     * @var bool
     */
    protected $published;

    public function __construct()
    {
        $this->published = false;
        $this->creationDate = new \DateTime();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getShortContent() {
        return $this->shortContent;
    }

    /**
     * @return string
     */
    public function getShortTitle()
    {
        return empty($this->shortTitle) ? $this->title : $this->shortTitle;
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        return empty($this->metaTitle) ? $this->title : $this->metaTitle;
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function getPicture()
    {
        return empty($this->picture) ? null : $this->getUploadDir() . $this->picture;
    }

    /**
     * {@inheritdoc}
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * {@inheritdoc}
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * {@inheritdoc}
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setShortContent($shortContent) {
        $this->shortContent = $shortContent;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function publish($published)
    {
        $this->published = $published;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdateDate()
    {
        $this->updateDate = new \DateTime();
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setShortTitle($shortTitle)
    {
        $this->shortTitle = $shortTitle;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function savePicture(UploadedFile $file)
    {
        if (null === $file) {
            return;
        }

        $filename = sha1(uniqid(mt_rand(), true)) . '.' . $file->guessExtension();

        $file->move('../web/' . $this->getUploadDir(), $filename);

        $this->removePicture();

        $this->picture = $filename;

    }

    /**
     * {@inheritdoc}
     */
    public function removePicture()
    {
        if ($this->getPicture() && is_file('../web/' . $this->getPicture())) {
            unlink('../web/' . $this->getPicture());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUploadDir()
    {
        return 'uploads/';
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

}