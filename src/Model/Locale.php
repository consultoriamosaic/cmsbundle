<?php

namespace Mosaic\CMSBundle\Model;


class Locale implements LocaleInterface
{

    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var bool
     */
    protected $enabled;

    /**
     * Locale constructor.
     * @param string $code
     */
    public function __construct($code)
    {
        $this->code = $this->canonicalizeCode($code);
        $this->enabled = false;
    }

    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function canonicalizeCode($code)
    {
        return trim($code);
    }

}