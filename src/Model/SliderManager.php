<?php

namespace Mosaic\CMSBundle\Model;

abstract class SliderManager implements SliderManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function createSliderPhoto()
    {
        $class = $this->getClass();
        $sliderPhoto = new $class;
        return $sliderPhoto;
    }

    /**
     * {@inheritdoc}
     */
    public function findSliderPhotoById($id)
    {
        return $this->findSliderPhotoBy(['id' => $id]);
    }

}