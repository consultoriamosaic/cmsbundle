<?php

namespace Mosaic\CMSBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

interface GalleryInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getMachineName();

    /**
     * @return boolean
     */
    public function isEnabled();

    /**
     * @return ArrayCollection
     */
    public function getSliderPhotos();
}