<?php
namespace Mosaic\CMSBundle\Model;

interface NewsManagerInterface
{
    /**
     * Creates a news.
     * @return NewsInterface
     */
    public function createNews();

    /**
     * Deletes a news.
     *
     * @param NewsInterface $news
     *
     * @return void
     */
    public function deleteNews(NewsInterface $news);

    /**
     * Returns the news's fully qualified class name.
     *
     * @return string
     */
    public function getClass();

    /**
     * Updates a news.
     *
     * @param NewsInterface $news
     *
     * @return void
     */
    public function updateNews(NewsInterface $news);

    /**
     * Finds one news by the given criteria.
     *
     * @param array $criteria
     *
     * @return NewsInterface or null
     */
    public function findNewsBy(array $criteria);

    /**
     * Finds one news by its slug.
     *
     * @param string $newsId
     * @param bool $published if true return public news only, false ignores that field
     *
     * @return NewsInterface or null
     */
    public function findnewsById($newsId, $published = true);

    /**
     * Returns a collection with all news instances.
     * @param bool $published if true return public news only, false ignores that field
     *
     * @return \Traversable
     */
    public function findAllNews($published = true);

    /**
     * Check if a news with given slug already exists.
     *
     * @param string $slug
     * @return bool true if slug already exists in database
     */
    public function checkSlug($slug);

    /**
     * Normalizes the slug of a given News
     *
     * @param NewsInterface $news
     * @return NewsInterface
     */
    public function normalizeSlug(NewsInterface $news);
}