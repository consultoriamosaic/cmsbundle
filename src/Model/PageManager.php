<?php

namespace Mosaic\CMSBundle\Model;

abstract class PageManager implements PageManagerInterface
{

    /**
     * {@inheritdoc}
     */
    public function createPage()
    {
        $class = $this->getClass();
        $page = new $class;
        return $page;
    }

    /**
     * {@inheritdoc}
     */
    public function findPageByMachineName($machineName, $public = true)
    {
        $criteria = [
            'machineName' => $machineName
        ];

        if ($public) {
            $criteria['public'] = true;
        }

        return $this->findPageBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function checkMachineName($machineName)
    {
        return $this->findPageByMachineName($machineName, false) != null;
    }
}