<?php

namespace Mosaic\CMSBundle\Model;

interface PageManagerInterface
{

    /**
     * Creates a page.
     * @return PageInterface
     */
    public function createPage();

    /**
     * Deletes a page.
     *
     * @param PageInterface $page
     *
     * @return void
     */
    public function deletePage(PageInterface $page);

    /**
     * Returns the page's fully qualified class name.
     *
     * @return string
     */
    public function getClass();

    /**
     * Updates a page.
     *
     * @param PageInterface $page
     *
     * @return void
     */
    public function updatePage(PageInterface $page);

    /**
     * Finds one page by the given criteria.
     *
     * @param array $criteria
     *
     * @return PageInterface or null
     */
    public function findPageBy(array $criteria);

    /**
     * Finds one page by its machineName.
     *
     * @param string $machineName
     * @param bool $public if true return public pages only, false ignores that field
     *
     * @return PageInterface or null
     */
    public function findPageByMachineName($machineName, $public = true);

    /**
     * Returns a collection with all pages instances.
     *
     * @return \Traversable
     */
    public function findAllPages();

    /**
     * Check if a page with given machineName already exists.
     *
     * @param string $machineName
     * @return bool true if machineName already exists in database
     */
    public function checkMachineName($machineName);

    /**
     * Gets a page from the slug, public service.
     * 
     * @param $slug
     * @return PageInterface
     */
    public function getPage($slug);

    /**
     * @param string $machineNamePrefix
     * @return PageInterface|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPageByMachineNamePrefix($machineNamePrefix);

    /**
     * @param string $machineName
     * @return array
     */
    public function getAssociatedBlocks($machineName);

}