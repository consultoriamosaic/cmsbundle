<?php

namespace Mosaic\CMSBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class SliderPhoto implements SliderPhotoInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $photo;

    /**
     * @var int
     */
    protected $position;

    /**
     * @var boolean
     */
    protected $public;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $link;

    /**
     * @var string
     */
    protected $alt;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @var GalleryInterface
     */
    protected $gallery;

    /**
     * SliderPhoto constructor.
     */
    public function __construct()
    {
        $this->public = false;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return empty($this->photo) ? null : $this->getUploadDir() . $this->photo;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return boolean
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return GalleryInterface
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @param boolean $public
     */
    public function setPublic($public)
    {
        $this->public = $public;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @param string $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * {@inheritdoc}
     */
    public function savePhoto($file)
    {
        if (null === $file) {
            return;
        }

        $filename = sha1(uniqid(mt_rand(), true)) . '.' . $file->guessExtension();

        $file->move('../web/' . $this->getUploadDir(), $filename);

        $this->removePhoto();

        $this->photo = $filename;

    }

    /**
     * {@inheritdoc}
     */
    public function removePhoto()
    {
        if ($this->getPhoto() && is_file('../web/' . $this->getPhoto())) {
            unlink('../web/' . $this->getPhoto());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUploadDir()
    {
        return 'uploads/';
    }

    public function setGallery(GalleryInterface $gallery)
    {
        $this->gallery = $gallery;
    }

}