<?php

namespace Mosaic\CMSBundle\Model;

abstract class LocaleManager implements LocaleManagerInterface
{

    /**
     * {@inheritdoc}
     */
    public function createLocale($code)
    {
        return new Locale($code);
    }

    /**
     * {@inheritdoc}
     */
    public function findLocaleByCode($code)
    {
        return $this->findLocaleBy(array('code' => $code));
    }
}