<?php

namespace Mosaic\CMSBundle\Model;


abstract class NewsManager implements NewsManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function createNews()
    {
        $class = $this->getClass();
        $news = new $class;
        return $news;
    }

    /**
     * {@inheritdoc}
     */
    public function findNewsById($newsId, $published = true)
    {
        $criteria = [
            'id' => $newsId
        ];

        if ($published) {
            $criteria['published'] = true;
        }

        return $this->findNewsBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function checkSlug($slug)
    {
        return $this->findNewsBy(['slug' => $slug]) != null;
    }
}