<?php

namespace Mosaic\CMSBundle\Model;

interface LocaleManagerInterface
{
    /**
     * Creates a Locale.
     *
     * @param string $code
     * @return LocaleInterface
     */
    public function createLocale($code);

    /**
     * Deletes a Locale.
     *
     * @param LocaleInterface
     * @return void
     */
    public function deleteLocale(LocaleInterface $locale);

    /**
     * Enables a Locale.
     *
     * @param LocaleInterface
     * @return void
     */
    public function enableLocale(LocaleInterface $locale);

    /**
     * Disables a Locale.
     *
     * @param LocaleInterface
     * @return void
     */
    public function disableLocale(LocaleInterface $locale);

    /**
     * Updates a Locale.
     *
     * @param LocaleInterface $locale
     * @return void
     */
    public function updateLocale(LocaleInterface $locale);

    /**
     * Gets all enabled Locales.
     *
     * @return \Traversable
     */
    public function getEnabledLocales();

    /**
     * Gets all Locales.
     *
     * @return \Traversable
     */
    public function getAllLocales();

    /**
     * Finds a Locale by criteria.
     *
     * @param array $criteria
     * @return LocaleInterface
     */
    public function findLocaleBy($criteria);

    /**
     * Finds a Locale by its code.
     *
     * @param string $code
     * @return LocaleInterface
     */
    public function findLocaleByCode($code);

    /**
     * Returns the locale's fully qualified class name.
     *
     * @return string
     */
    public function getClass();
}