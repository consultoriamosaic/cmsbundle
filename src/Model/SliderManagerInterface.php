<?php

namespace Mosaic\CMSBundle\Model;

interface SliderManagerInterface
{
    /**
     * Creates a sliderPhoto.
     * @return SliderPhotoInterface
     */
    public function createSliderPhoto();

    /**
     * Returns the SliderPhoto's fully qualified class name.
     *
     * @return string
     */
    public function getClass();

    /**
     * Deletes a SliderPhoto.
     *
     * @param SliderPhotoInterface $sliderPhoto
     *
     * @return void
     */
    public function deleteSliderPhoto(SliderPhotoInterface $sliderPhoto);

    /**
     * Updates a SliderPhoto.
     *
     * @param SliderPhotoInterface $sliderPhoto
     *
     * @return void
     */
    public function updateSliderPhoto(SliderPhotoInterface $sliderPhoto);

    /**
     * Finds one SliderPhoto by the given criteria.
     *
     * @param array $criteria
     *
     * @return SliderPhotoInterface or null
     */
    public function findSliderPhotoBy(array $criteria);

    /**
     * Finds one SliderPhoto by its id.
     *
     * @param string $id
     *
     * @return SliderPhotoInterface or null
     */
    public function findSliderPhotoById($id);

    /**
     * Returns a collection with all SliderPhoto instances.
     *
     * @param bool $public
     * 
     * @return \Traversable
     */
    public function findAllSlides($public = true);

}