<?php

namespace Mosaic\CMSBundle\Model;

interface PageInterface
{

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getSlug();

    /**
     * @return string
     */
    public function getMachineName();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getShortTitle();

    /**
     * @return string
     */
    public function getContent();

    /**
     * @return string
     */
    public function getMetaTitle();

    /**
     * @return string
     */
    public function getMetaDescription();

    /**
     * @return string
     */
    public function getMetaKeywords();

    /**
     * @return string
     */
    public function getLocale();

    /**
     * @return boolean
     */
    public function isPublic();

    /**
     * Sets the title.
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title);

    /**
     * Sets the slug.
     *
     * @param $slug
     * @return self
     */
    public function setSlug($slug);

    /**
     * Sets the content.
     *
     * @param $content
     * @return self
     */
    public function setContent($content);

    /**
     * Sets the meta description.
     *
     * @param $metaDescription
     * @return self
     */
    public function setMetaDescription($metaDescription);

    /**
     * Sets the meta keywords.
     *
     * @param $metaKeywords
     * @return self
     */
    public function setMetaKeywords($metaKeywords);

    /**
     * Sets public.
     *
     * @param boolean $public
     * @return self
     */
    public function setPublic($public);

    /**
     * Updates the lastUpdate with now.
     */
    public function setUpdateDate();

    /**
     * Sets short title.
     *
     * @param string $shortTitle
     * @return self
     */
    public function setShortTitle($shortTitle);

    /**s
     * Sets meta title.
     *
     * @param string $metaTitle
     * @return self
     */
    public function setMetaTitle($metaTitle);

    /**
     * Sets the machine name.
     * 
     * @param string $machineName
     * @return self
     */
    public function setMachineName($machineName);

    /**
     * Sets the locale.
     * 
     * @param $localeCode
     * @return self
     */
    public function setLocale($localeCode);

}