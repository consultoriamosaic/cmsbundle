<?php

namespace Mosaic\CMSBundle\Model;


interface LocaleInterface
{

    /**
     * Gets the locale code.
     * 
     * @return string
     */
    public function getCode();

    /**
     * Gets enabled state.
     * 
     * @return bool
     */
    public function isEnabled();

    /**
     * Sets enabled state.
     * 
     * @param bool $enabled
     * @return self
     */
    public function setEnabled($enabled);

    /**
     * Canonicalize a Locale code.
     *
     * @param string $code
     * @return string canonicalized code.
     */
    public function canonicalizeCode($code);
    
}