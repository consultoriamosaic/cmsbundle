<?php

namespace Mosaic\CMSBundle\Util;

use Mosaic\CMSBundle\Model\LocaleManagerInterface;

class LocaleManipulator
{
    /**
     * @var LocaleManagerInterface
     */
    private $localeManager;

    /**
     * LocaleManipulator constructor.
     * @param LocaleManagerInterface $localeManager
     */
    public function __construct(LocaleManagerInterface $localeManager)
    {
        $this->localeManager = $localeManager;
    }

    /**
     * Creates a locale and returns it.
     * 
     * @param string $code
     * @param bool $enabled
     * @return \Mosaic\CMSBundle\Model\LocaleInterface
     */
    public function create($code, $enabled = false)
    {
        $locale = $this->localeManager->createLocale($code);
        if($enabled) {
            $this->localeManager->enableLocale($locale);
        }
        $this->localeManager->updateLocale($locale);
        return $locale;
    }

    /**
     * Checks if a locale already has a code.
     * 
     * @param string $code
     * @return bool true if there's already a locale with that code.
     */
    public function checkLocaleExists($code)
    {
        $locale = $this->localeManager->findLocaleByCode($code);
        $localeClass = $this->localeManager->getClass();

        return $locale instanceof $localeClass;
    }

    /**
     * Deletes a Locale by its code.
     *
     * @param string $code
     * @return bool true if Locale was found.
     */
    public function delete($code)
    {
        $locale = $this->localeManager->findLocaleByCode($code);

        $localeClass = $this->localeManager->getClass();

        if($locale instanceof $localeClass) {
            $this->localeManager->deleteLocale($locale);
            return true;
        }

        return false;
    }

    /**
     * Gets all Locales.
     *
     * @return \Traversable
     */
    public function getAll()
    {
        return $this->localeManager->getAllLocales();
    }

    /**
     * Sets enabled state of a Locale.
     *
     * @param string $code
     * @param bool $enabled
     * @return bool true if found and deleted
     */
    public function enable($code, $enabled)
    {
        $locale = $this->localeManager->findLocaleByCode($code);
        $localeClass = $this->localeManager->getClass();

        if($locale instanceof $localeClass) {
            if($enabled) {
                $this->localeManager->enableLocale($locale);
            } else {
                $this->localeManager->disableLocale($locale);
            }
            return true;
        }

        return false;
    }
}

