<?php

namespace Mosaic\CMSBundle\Util;

use Mosaic\CMSBundle\Model\PageManagerInterface;

class PageManipulator
{

    /**
     * @var PageManagerInterface
     */
    private $pageManager;

    /**
     * PageManipulator constructor.
     * @param PageManagerInterface $pageManager
     */
    public function __construct(PageManagerInterface $pageManager)
    {
        $this->pageManager = $pageManager;
    }

    /**
     * Creates a page and returns it.
     *
     * @param string $title
     * @param string $machineName
     * @return \Mosaic\CMSBundle\Model\PageInterface
     */
    public function create($title, $machineName)
    {
        $page = $this->pageManager->createPage();
        $page->setTitle($title);
        $page->setMachineName($machineName);

        $this->pageManager->updatePage($page);

        return $page;
    }

    /**
     * Deletes a page by its machineName.
     *
     * @param string $machineName
     * @return bool true if page was found.
     */
    public function delete($machineName)
    {
        $page = $this->pageManager->findPageByMachineName($machineName, false);
        $pageClass = $this->pageManager->getClass();

        if ($page instanceof $pageClass) {
            $this->pageManager->deletePage($page);
            return true;
        }

        return false;
    }

    /**
     * Finds all pages.
     *
     * @return \Traversable
     */
    public function getAll()
    {
        return $this->pageManager->findAllPages();
    }

    /**
     * Sets the public state of a page.
     *
     * @param string $machineName
     * @param bool $public
     * @return bool true if page was found.
     */
    public function publish($machineName, $public)
    {
        $page = $this->pageManager->findPageByMachineName($machineName, false);
        $pageClass = $this->pageManager->getClass();

        if ($page instanceof $pageClass) {
            $page->setPublic($public);
            $this->pageManager->updatePage($page);
            return true;
        }

        return false;
    }

    /**
     * @param string $machineName
     * @return bool
     */
    public function checkMachineName($machineName)
    {
        return $this->pageManager->checkMachineName($machineName);
    }

}   