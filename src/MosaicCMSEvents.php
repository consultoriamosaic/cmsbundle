<?php

namespace Mosaic\CMSBundle;

final class MosaicCMSEvents
{
    /**
     * Occurs when the page manager doesn't find a page.
     */
    const PAGE_NOT_FOUND = 'mosaic_cms.page.not_found';
}