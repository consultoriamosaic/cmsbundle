<?php

namespace Mosaic\CMSBundle;

use Mosaic\CMSBundle\DependencyInjection\Compiler\OrmResolverCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;

class MosaicCMSBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $modelDir = realpath(__DIR__ . '/Resources/config/doctrine-mappings');
        $mappings = array(
            $modelDir => 'Mosaic\CMSBundle\Model',
        );

        $ormCompilerClass = 'Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass';
        if (class_exists($ormCompilerClass)) {
            $container->addCompilerPass(
                DoctrineOrmMappingsPass::createYamlMappingDriver(
                    $mappings,
                    array('mosaic_cms.model_manager_name'),
                    'mosaic_cms.backend_type_orm',
                    array('MosaicCMSBundle' => 'Mosaic\CMSBundle\Model')
                ));
        }
    }

}
