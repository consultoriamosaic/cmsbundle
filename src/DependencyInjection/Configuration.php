<?php

namespace Mosaic\CMSBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('mosaic_cms');

        // for now
        $supportedDrivers = ['orm'];

        // general config + gedmo config
        $rootNode
            ->children()
            ->scalarNode('db_driver')
            ->validate()
            ->ifNotInArray($supportedDrivers)
            ->thenInvalid('The driver %s is not supported. Please choose one of '.json_encode($supportedDrivers))
            ->end()
            ->cannotBeOverwritten()
            ->isRequired()
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('default_locale')->cannotBeEmpty()->end()
            ->booleanNode('translation_fallback')->defaultFalse()->end()
            ->booleanNode('persist_default_translation')->defaultTrue()->end()
            ->booleanNode('skip_translation_on_load')->defaultFalse()->end()
            ->end();

        // page config
        $rootNode
            ->children()
            ->arrayNode('page')
            ->canBeEnabled()
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('class')->isRequired()->cannotBeEmpty()->end()
            ->scalarNode('404')->cannotBeEmpty()->defaultValue('Mosaic\CMSBundle\Listener\PageNotFoundListener')->end()
            ->end()
            ->end()
            ->end();

        // news config
        $rootNode
            ->children()
            ->arrayNode('news')
            ->canBeEnabled()
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('class')->isRequired()->cannotBeEmpty()->end()
            ->integerNode('news_per_page')->defaultValue(5)->end()
            ->end()
            ->end()
            ->end();

        // contact config
        $rootNode
            ->children()
            ->arrayNode('contact')
            ->canBeEnabled()
            ->addDefaultsIfNotSet()
            ->end()
            ->end();

        // gallery config
        $rootNode
            ->children()
            ->arrayNode('gallery')
            ->canBeEnabled()
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('class')->isRequired()->cannotBeEmpty()->end()
            ->end()
            ->end()
            ->end();

        // slider config <- if gallery is enabled, slider also has to be
        $rootNode
            ->children()
            ->arrayNode('slider')
            ->canBeEnabled()
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('class')->isRequired()->cannotBeEmpty()->end()
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
