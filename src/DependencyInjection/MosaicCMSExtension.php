<?php

namespace Mosaic\CMSBundle\DependencyInjection;

use Mosaic\CMSBundle\MosaicCMSEvents;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Extension\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class MosaicCMSExtension extends Extension
{

    private static $doctrineDrivers = array(
        'orm' => array(
            'registry' => 'doctrine',
            'tag'      => 'doctrine.event_subscriber',
        )
    );

    private static $internalMappings = array(
        'page'    => 'page/',
        'news'    => 'news/',
        'slider'  => 'slider/',
        'gallery' => 'gallery/'
    );

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $processedConfig = $this->processConfiguration($configuration, $configs);

        $container->setParameter('mosaic_cms.page.not_found_listener_class', $processedConfig['page']['404']);

        $container->setParameter('mosaic_cms.default_locale', $processedConfig['default_locale']);
        $container->setParameter('mosaic_cms.translation_fallback', $processedConfig['translation_fallback']);
        $container->setParameter('mosaic_cms.persist_default_translation',
            $processedConfig['persist_default_translation']);
        $container->setParameter('mosaic_cms.skip_translation_on_load', $processedConfig['skip_translation_on_load']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        if ('custom' !== $processedConfig['db_driver']) {
            if (isset(self::$doctrineDrivers[$processedConfig['db_driver']])) {
                $loader->load('doctrine.yml');
                $loader->load('util.yml');
                $container->setAlias('mosaic_cms.doctrine_registry',
                    new Alias(self::$doctrineDrivers[$processedConfig['db_driver']]['registry'], false));
            } else {
                $loader->load(sprintf('%s.yml', $processedConfig['db_driver']));
            }
            $container->setParameter($this->getAlias() . '.backend_type_' . $processedConfig['db_driver'], true);
        }

        // Configure the factory for both Symfony 2.3 and 2.6+
        if (isset(self::$doctrineDrivers[$processedConfig['db_driver']])) {
            $definition = $container->getDefinition('mosaic_cms.object_manager');
            if (method_exists($definition, 'setFactory')) {
                $definition->setFactory(array(new Reference('mosaic_cms.doctrine_registry'), 'getManager'));
            } else {
                $definition->setFactoryService('mosaic_cms.doctrine_registry');
                $definition->setFactoryMethod('getManager');
            }
        }

        if ($processedConfig['page']['enabled']) {

            $container->setParameter('mosaic_cms.page_class', $processedConfig['page']['class']);

            // loads doctrine
            if (isset(self::$doctrineDrivers[$processedConfig['db_driver']])) {
                $loader->load(self::$internalMappings['page'] . 'doctrine.yml');
            }

            // loads util (page manipulator for commands)
            $loader->load(self::$internalMappings['page'] . 'util.yml');

            // loads predefined forms
            $loader->load(self::$internalMappings['page'] . 'forms.yml');
        }

        if ($processedConfig['news']['enabled']) {

            $container->setParameter('mosaic_cms.news_class', $processedConfig['news']['class']);
            $container->setParameter('mosaic_cms.news_npp', $processedConfig['news']['news_per_page']);

            // loads doctrine
            if (isset(self::$doctrineDrivers[$processedConfig['db_driver']])) {
                $loader->load(self::$internalMappings['news'] . 'doctrine.yml');
            }

            // loads predefined forms
            $loader->load(self::$internalMappings['news'] . 'forms.yml');
        }

        // enabled gallery + slider
        if ($processedConfig['gallery']['enabled']) {

            $container->setParameter('mosaic_cms.slider_class', $processedConfig['slider']['class']);
            $container->setParameter('mosaic_cms.gallery_class', $processedConfig['gallery']['class']);

            // loads doctrine
            $loader->load(self::$internalMappings['gallery'] . 'doctrine.yml');
            $loader->load(self::$internalMappings['slider'] . 'doctrine.yml');

            // loads predefined forms
            $loader->load(self::$internalMappings['gallery'] . 'forms.yml');
            $loader->load(self::$internalMappings['slider'] . 'forms.yml');
        }

        if ($processedConfig['contact']['enabled']) {

            // loads predefined contact form
            $loader->load('contact.yml');

        }
    }

}
