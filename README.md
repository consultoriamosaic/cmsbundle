### pending
- config:
    - default locale check (undef index)
    - check classes exist
- add/disable fields in contacttype options (validations)
- validations on dynamic field mapping
- slug canonalizer
- override repos
- add fields to predefined form dynamically from extended page/news class
- page groups
- test on sf 2.7
- controllers
- update manager tests + forms tests
- validation override ?
- make slider extensible as page and news

### commands
- mosaic:
   - cms:
       - page:
           - list
           - create [title:string, slug:string]
           - delete [slug:string]
           - publish [slug:string, public:Yes/No]
           - preload [file:string] => yml file, 1 line per page to create as ```title: machine_name```
       - locale:
           - list
           - create [code:string, enable:Yes/No]
           - delete [code:string]
           - enable [code:string, enable:Yes/No]

### todo: doc
https://symfony.com/doc/current/cookbook/bundles/best_practices.html#installation-instructions

#### require
```"mosaic/cms": "dev-master"```

```
"repositories": [
         {
           "type": "vcs",
           "url": "git@bitbucket.org:consultoriamosaic/cmsbundle.git"
         }
       ]
```

#### // app/config/config.yml
```
- doctrine.orm.mappings:
    - MosaicCMSBundle: false || doctrine.orm.auto_mapping: false
    - translatable:
        - type: annotation
        - alias: gedmo
        - prefix: Gedmo\Translatable\Entity
        - dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translatable/Entity"

- mosaic_cms:
    - db_driver: 'orm' # only orm available now
    - page: # only if page must be activated
        # - enabled: false (default)
        - class: '\AppBundle\Model\Page'
    - news: # only if news must be activated
        # - enabled: false (default)
        - class: '\AppBundle\Model\News
    - contact:
        # - enabled: false (default)
        - 404: 'Mosaic\CMSBundle\Listener\PageNotFoundListener'
    - slider:
        # -enabled: false (default)
    - default_locale: "%locale%"
    - translation_fallback: false
    - persist_default_translation: true
    - skip_translation_on_load: false
```
#### // app/config/routing.yml

(next versions)
```
- mosaic:
    - resource: "@MosaicCMSBundle/Controller/"
    - type:     annotation
    - prefix:   /mosaic_cms
```
### todo: services
